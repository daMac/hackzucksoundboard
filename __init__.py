import st3m.run, leds, bl00mbox

from st3m.application import Application, ApplicationContext
from st3m.ui.view import BaseView, ViewManager
from st3m.input import InputState, InputController
from ctx import Context

class HackZuckSoundBoard(Application):
    input: InputController
    channel: bl00mbox.Channel
    effects = {}
    effects_list = dict([
        (0, "red-alert.wav"),
        (2, "falsch.wav"),
        (4, "5sweiter.wav"),
        (6, "richtig.wav"),
        (8, "zeit.wav")
    ])
    is_loading: bool = True

    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.input = InputController()
        self.channel = bl00mbox.Channel("McSoundBoard")
        self._load_effect()

    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        # Paint a red square in the middle of the display
        #ctx.rgb(1, 1, 0).rectangle(-20, -20, 40, 40).fill()

        # https://docs.flow3r.garden/badge/programming.html#transferring-files-over-repl

        # Draw a image file
        if(self.app_ctx.bundle_path == None):
            ctx.image("/flash/sys/apps/hackzuck-soundboard/hack-zuck.png", -120, -120, 240, 240)
        else:
            ctx.image(f"{self.app_ctx.bundle_path}/hack-zuck.png", -120, -120, 240, 240)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        if self.is_loading == True:
            return

        #Petals 0, 2, 4, 6, 8 are Top petals.
        #Petals 1, 3, 5, 7, 9 are Bottom petals.
        
        for k, v in self.effects.items():
            if self.input.captouch.petals[k].whole.pressed:
                leds.set_all_rgb(1/(k+10), 0, 1/(k+22))
                leds.update()
                self.effects[k].signals.trigger.start()


    def _load_effect(self) -> None:
        for k, v in self.effects_list.items():
            print(f"load {v}")
            if(self.app_ctx.bundle_path == None):
                sample = self.channel.new(bl00mbox.patches.sampler, f"/flash/sys/apps/hackzuck-soundboard/effects/{v}")
            else:
                sample = self.channel.new(bl00mbox.patches.sampler, f"{self.app_ctx.bundle_path}/effects/{v}")

            sample.signals.output = self.channel.mixer
            self.effects[k] = sample
            
        self.is_loading = False
        return

    def file_or_dir_exists(filename):
        try:
            os.stat(filename)
            return True
        except OSError:
            return False
        
if __name__ == '__main__':
    # Continue to make runnable via mpremote run.
    st3m.run.run_app(HackZuckSoundBoard)